package dev.cc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class DriverTest {

    @Test
    public void testFindHighestValue(){
        int[] numbers = {1,3,2};
        Assertions.assertEquals(3, Driver.findHighestValue(numbers));
    }

    @Test
    public void testFindSecondHighestValue(){
        int[] numbers = {4,6,1,3,9};
        Assertions.assertEquals(6, Driver.findSecondHighestValue(numbers));
    }
}
