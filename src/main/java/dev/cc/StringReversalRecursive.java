package dev.cc;

public class StringReversalRecursive {

    public static void main(String[] args) {

    }

    public static String recursiveStringReversal(String input){
        if(input.length() == 1)
            return input;
        else {
            System.out.println(input);
            return input.charAt(input.length() - 1) + recursiveStringReversal(input.substring(0, input.length() - 1));
        }
    }
}
