package dev.cc;

public class Palindrome {

    public static void main(String[] args) {
        String test1 = "solos";
        String test2 = "Solos";
        String test3 = "Red rum, sir, is murder";
        System.out.println("Checking if '" + test1 + "' could be considered a palindrome.");
        System.out.println("\tTrue Palindrome:\t\t\t" + checkIfPalindrome(test1));
        System.out.println("\tCaseless Palindrome:\t\t" + checkIfCaselessPalindrome(test1));
        System.out.println("\tLetter Palindrome:\t\t\t" + checkIfLetterPalindrome(test1));
        System.out.println("\n\n");

        System.out.println("Checking if '" + test2 + "' could be considered a palindrome.");
        System.out.println("\tTrue Palindrome:\t\t\t" + checkIfPalindrome(test2));
        System.out.println("\tCaseless Palindrome:\t\t" + checkIfCaselessPalindrome(test2));
        System.out.println("\tLetter Palindrome:\t\t\t" + checkIfLetterPalindrome(test2));
        System.out.println("\n\n");

        System.out.println("Checking if '" + test3 + "' could be considered a palindrome.");
        System.out.println("\tTrue Palindrome:\t\t\t" + checkIfPalindrome(test3));
        System.out.println("\tCaseless Palindrome:\t\t" + checkIfCaselessPalindrome(test3));
        System.out.println("\tLetter Palindrome:\t\t\t" + checkIfLetterPalindrome(test3));
        System.out.println("\n\n");
    }

    // I am calling a string a 'True Palindrome' if the string is the same when reversed,
    // including case. i.e. solos but not Solos
    private static boolean checkIfPalindrome(String input){
        String inputReversed = StringReversal.reverseString(input);
        return input.equals(inputReversed);
    }

    // I am calling a string a 'Caseless Palindrome' if the string is a case-insensitive
    // palindrome. i.e. solos and Solos
    private static boolean checkIfCaselessPalindrome(String input){
        input = input.toLowerCase();
        String inputReversed = StringReversal.reverseString(input);
        return input.equals(inputReversed);
    }

    // I am calling a string a 'Letter Palindrome' if the letters without spaces or punctuation
    // form a palindrome. The resulting reversed string may need spaces and punctuation to make
    // sense. Note that this is also case-insensitive. i.e. Red rum, sir, is murder
    private static boolean checkIfLetterPalindrome(String input){
        StringBuilder simplifiedString = new StringBuilder();
        for(int i = 0; i < input.length(); i++){
            if(Character.isAlphabetic(input.charAt(i)))
                simplifiedString.append(Character.toLowerCase(input.charAt(i)));
        }
        String inputReversed = StringReversal.reverseString(simplifiedString.toString());
        return inputReversed.equals(simplifiedString.toString());
    }
}
