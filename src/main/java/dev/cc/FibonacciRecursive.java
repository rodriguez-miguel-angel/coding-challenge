package dev.cc;

import java.util.Arrays;

public class FibonacciRecursive {

    public static void main(String[] args) {

    }

    public static int findNthFibonacciNumber(int n){
        if(n <= 0){
            return n;
        }
        switch (n){
            case 1:
                return 0;
            case 2:
                return 1;
            default:
                int[] starter = {0,1};
                int[] fSequence = recursiveCalculate(starter,n);
                return fSequence[fSequence.length - 1];
        }
    }

    public static int[] recursiveCalculate(int[] fSequence, int n){
        if(fSequence.length < n){
            int[] temp = new int[fSequence.length + 1];
            System.arraycopy(fSequence,0,temp,0,fSequence.length);
            temp[fSequence.length] = fSequence[fSequence.length-2] + fSequence[fSequence.length-1];
            fSequence = recursiveCalculate(temp, n);
        }
        return fSequence;
    }
}
