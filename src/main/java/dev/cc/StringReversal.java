package dev.cc;

public class StringReversal {

    public static void main(String[] args) {
        String input = "red rum sir is murder";
        System.out.println("The word '" + input + "' reversed is '" + reverseString(input) + "'.");
    }

    public static String reverseString(String input){
        int numChars = input.length();
        char[] reversedInput = new char[numChars];
        for(int i = 0; i < numChars; i++){
            reversedInput[i] = input.charAt(numChars - i - 1);
        }
        return new String(reversedInput);
    }
}
