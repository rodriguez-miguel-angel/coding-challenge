package dev.cc.customStack;

public class Node {

    private Object contents;
    private Node previous = null;

    public Node(Object contents, Node previous) {
        this.contents = contents;
        this.previous = previous;
    }

    public Node(Object contents) {
        this.contents = contents;
    }

    public Node() {
    }

    public Object getContents() {
        return contents;
    }

    public void setContents(Object contents) {
        this.contents = contents;
    }

    public Node getPrevious() {
        return previous;
    }

    public void setPrevious(Node previous) {
        this.previous = previous;
    }
}
