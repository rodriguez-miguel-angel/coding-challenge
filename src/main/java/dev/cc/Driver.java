package dev.cc;

public class Driver {

    public static void main(String[] args) {

    }

    public static int findSecondHighestValue(int[] numbers){
        int highestValue = findHighestValue(numbers);
        int secondHighestValue = 0;
        boolean highestMatched = false;
        for(int number: numbers){
            if(number == highestValue && !highestMatched) {
                highestMatched = true;
                continue;
            }
            if(number >= secondHighestValue){
                secondHighestValue = number;
            }
        }
        return secondHighestValue;
    }


    public static int findHighestValue(int[] numbers){
        int highestValue = 0;
        for(int number: numbers){
            if(number > highestValue)
                highestValue = number;
        }
        return highestValue;
    }
}
