package dev.cc;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class FibonacciRecursiveTest {

    @Test
    public void testRecursiveCalculate(){
        int[] originalArray = {0,1};
        int n = 10;
        int[] finalArray = {0,1,1,2,3,5,8,13,21,34};
        int[] fSequence = FibonacciRecursive.recursiveCalculate(originalArray, n);
        for(int i = 0; i < fSequence.length; i++){
            Assertions.assertEquals(finalArray[i],fSequence[i]);
        }
    }

    @Test
    public void testFindNthFibonacciNumberNegative(){
        Assertions.assertAll(
                ()-> Assertions.assertEquals(0,FibonacciRecursive.findNthFibonacciNumber(0)),
                ()-> Assertions.assertEquals(-10,FibonacciRecursive.findNthFibonacciNumber(-10))
        );
    }

    @Test
    public void testFindNthFibonacciNumberSmall(){
        Assertions.assertEquals(1,FibonacciRecursive.findNthFibonacciNumber(2));
    }

    @Test
    public void testFindNthFibonacciNumber21st(){
        Assertions.assertEquals(6765, FibonacciRecursive.findNthFibonacciNumber(21));
    }
}
