package dev.cc.customStack;

public class CustomStack {

    private Node head = new Node();

    public Node getHead() {
        return head;
    }

    public int length(){
        if(head.getContents() == null)
            return 0;
        int length = 1;
        Node currentNode = head;
        while(currentNode.getPrevious() != null){
            length++;
            currentNode = currentNode.getPrevious();
        }
        return length;
    }

    public boolean push(Object o){
        if(head.getContents() == null){
            head.setContents(o);
            return true;
        } else {
            Node newHead = new Node(o,head);
            head = newHead;
            return true;
        }
    }

    public Object peek(){
        return head.getContents();
    }

    public Node pop(){
        if(head.getContents() == null)
            throw new NullPointerException();
        Node popped = head;
        head = head.getPrevious();
        return popped;
    }

    public int search(Object o){
        int position = 1;
        Node currentNode = head;
        do{
            if(currentNode.getContents().equals(o))
                return position;
            if(currentNode.getPrevious() == null)
                break;
            position++;
            currentNode = currentNode.getPrevious();
        } while (true);
        return 0;
    }
}
